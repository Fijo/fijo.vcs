using System;

namespace Fijo.VCS.Git.Exceptions {
	public class BlockedException : Exception {
		public object BlockedObject { get; set; }
		public BlockedException(object blockedObject) {
			BlockedObject = blockedObject;
		}
	}
}
using System;
using System.Collections.Generic;
using Fijo.VCS.Git.Dtos;
using Fijo.VCS.Git.Exceptions;
using Fijo.VCS.Git.Interfaces;

namespace Fijo.VCS.Git.Services {
	// ToDo
	//public class GitProcessStock : StockBase<GitService> {
	//    protected readonly Cache Content = new Cache();
			
	//    #region Overrides of StockBase<GitService>
	//    public override void Add(GitService item) {
	//        Content.Add(Guid.NewGuid().ToString(), item);
	//    }

	//    public override IEnumerable<GitService> GetAll() {
	//        throw new NotImplementedException();
	//    }

	//    public override void Remove(GitService item) {
	//        throw new NotImplementedException();
	//    }
	//    #endregion
	//}

	public class GitExecuteService : IGitExecuteService {
		public virtual bool IsAvailible(GitProcess process) {
			return !process.IsActive;
		}
		protected virtual void SetActive(GitProcess process, bool isActive) {
			process.IsActive = isActive;
		}
		protected virtual T InternalLock<T>(GitProcess process, Func<GitProcess, T> action) {
			lock(process) {
				if (!IsAvailible(process)) throw new BlockedException(process);
				SetActive(process, true);
			}
			T res;
			lock(process.Process) res = action(process);
			SetActive(process, false);
			return res;
		}
		protected void InternalLock(GitProcess process, Action<GitProcess> action) {
			InternalLock(process, proc => {
				action(proc);
				return default(bool);
			});
		}
		public virtual void Init(GitProcess process) {
			InternalLock(process, proc => proc.Process.Start());
		}
		public void Execute(GitProcess process, string command) {
			InternalLock(process, proc => InternalExecute(proc, command));
		}
		public void ExecuteMany(GitProcess process, IEnumerable<string> commands) {
			InternalLock(process, proc => InternalExecuteMany(proc, commands));
		}
		protected void InternalExecuteMany(GitProcess proc, IEnumerable<string> commands) {
			foreach (var command in commands) InternalExecute(proc, command);
		}
		protected virtual void InternalExecute(GitProcess process, string command) {
			process.Process.StandardInput.WriteLine(command);
		}
		public string ExecuteReturn(GitProcess process, string command) {
			return InternalLock(process, proc => {
				InternalExecute(proc, command);
				return ReadOutputStreamToEnd(proc);
			});
		}
		public string ExecuteManyReturn(GitProcess process, IEnumerable<string> commands) {
			return InternalLock(process, proc => {
				InternalExecuteMany(proc, commands);
				return ReadOutputStreamToEnd(proc);
			});
		}
		protected virtual string ReadOutputStreamToEnd(GitProcess process) {
			throw new NotImplementedException();
		}
		public virtual void Close(GitProcess process) {
			InternalLock(process, proc => proc.Process.Close());
		}
	}

	//[TestFixture]
	//public class GitExecutionServieTest {
	//    public GitProcessFactory.GitExecuteService _gitExecutionService;
	//    [SetUp]
	//    public void SetUp() {
	//        _gitExecutionService = new GitExecuteService();
	//    }
	//    [Test]
	//    public void Test() {
	//        _gitExecutionService.ExecuteReturn("");
	//    }
	//}
}
using System;
using System.Collections.Generic;
using System.Linq;
using Fijo.VCS.Git.Interfaces;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Cmd;

namespace Fijo.VCS.Git.Services {
	public class GitService : IGitService {
		private readonly ICommandService _commandService = Kernel.Resolve<ICommandService>();
		private readonly IGitExecutionFacade _gitExecutionFacade = Kernel.Resolve<IGitExecutionFacade>();

		public void Add(string repositoryDictionary, IEnumerable<string> files) {
			GitExec(repositoryDictionary, "add", string.Join(" ", files.Select(file => _commandService.WrapWithQuotes(_gitExecutionFacade.TranslatePath(file, false)))));
		}

		protected virtual void GitExec(string currentDictionary, string type, string args, params string[] inputs) {
			var command = string.Format(@"{0} {1}", type, args);
			Console.WriteLine("git {0}> {1}", currentDictionary, command);
			_gitExecutionFacade.Execute(currentDictionary, command, inputs ?? Enumerable.Empty<string>());
		}


		// ToDo add a methode for git bisect
		// ToDo add a methode for branch
		// ToDo add a methode for checkout
		// ToDo add a methode for diff
		// ToDo add a methode for fetch
		// ToDo add a methode for grep
		// ToDo add a methode for init
		// ToDo add a methode for log
		// ToDo add a methode for merge
		// ToDo add a methode for mv
		// ToDo add a methode for pull
		// ToDo add a methode for push
		// ToDo add a methode for rebase
		// ToDo add a methode for reset
		// ToDo add a methode for rm
		// ToDo add a methode for show
		// ToDo add a methode for status
		// ToDo add a methode for tag

		public void Clone(string dictionary, string repository, string checkoutDir) {
			GitExec(dictionary, "clone", string.Format("{0} {1}", _commandService.WrapWithQuotes(repository), _gitExecutionFacade.TranslatePath(checkoutDir, false)));
		}

		public void Commit(string repositoryDictionary, IEnumerable<string> files, string message) {
			GitExec(repositoryDictionary, string.Format(@"commit -m ""{0}""", _commandService.EscapeQuotes(message)), string.Join(string.Empty, files.Select(x => _commandService.WrapWithQuotes(_gitExecutionFacade.TranslatePath(x)))));
		}

		public void Push(string repositoryDictionary, string mirror, string repository) {
			GitExec(repositoryDictionary, "push", string.Format("{0} {1}", _commandService.WrapWithQuotes(mirror), _commandService.WrapWithQuotes(repository)));
		}
	}
}
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.Stock;
using Fijo.VCS.Git.Dtos;
using Fijo.VCS.Git.Interfaces;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Cmd;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;
using FijoCore.Infrastructure.LightContrib.Module.Path;
using JetBrains.Annotations;

namespace Fijo.VCS.Git.Services {
	public class GitExecutionFacade : IGitExecutionFacade {
		private readonly ICommandService _commandService = Kernel.Resolve<ICommandService>();
		private readonly IGitProcessFactory _gitProcessFactory = Kernel.Resolve<IGitProcessFactory>();
		private readonly IGitExecuteService _gitExecuteService = Kernel.Resolve<IGitExecuteService>();
		private readonly IStock<GitProcess> _store = Kernel.Resolve<IStock<GitProcess>>();
		private readonly IPathService _pathService = Kernel.Resolve<IPathService>();
		private readonly bool _useCygWin = Kernel.Resolve<IConfigurationService>().Get<bool>("Fijo.VCS.Git.UseCygWin");
		public virtual void Execute(string repoDict, string args, [NotNull] IEnumerable<string> inputs) {
			var avalible = GitAvalible();
			_gitExecuteService.ExecuteMany(avalible, GetCommands(repoDict, args, inputs));
		}
		[Pure]
		protected virtual IEnumerable<string> GetCommands(string repoDict, string args, [NotNull] IEnumerable<string> inputs) {
			yield return string.Format(@"cd ""{0}""", _commandService.EscapeQuotes(TranslatePath(repoDict)));
			yield return string.Format("git {0}", args);
			foreach (var input in inputs) yield return input;
		}
		[Pure]
		public string TranslatePath(string path, bool makeAbsolute = true) {
			var absolutePath = makeAbsolute && _pathService.IsRelative(path) ? Path.Combine(Environment.CurrentDirectory, path) : path;
			return _useCygWin ? _pathService.PathToCygWinPath(absolutePath) : absolutePath;
		}
		[NotNull]
		protected virtual GitProcess GitAvalible() {
			var avalible = _store.GetAll().Where(_gitExecuteService.IsAvailible).FirstOrDefault();
			if (avalible == null) _store.Add(avalible = Create());
			return avalible;
		}
		[NotNull]
		protected virtual GitProcess Create() {
			var process = _gitProcessFactory.Create();
			_gitExecuteService.Init(process);
			return process;
		}
	}
}
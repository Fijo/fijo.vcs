﻿using Fijo.Infrastructure.DesignPattern.Stock;
using Fijo.VCS.Git.Dtos;
using Fijo.VCS.Git.Factories;
using Fijo.VCS.Git.Interfaces;
using Fijo.VCS.Git.Services;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Properties;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;

namespace Fijo.VCS.Git.Properties {
	public class GitInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new LightContribInjectionModule());
		}

		public override void OnLoad(IKernel kernel) {
			kernel.Bind<IGitExecutionFacade>().To<GitExecutionFacade>().InSingletonScope();
			kernel.Bind<IGitProcessFactory>().To<GitProcessFactory>().InSingletonScope();
			kernel.Bind<IGitExecuteService>().To<GitExecuteService>().InSingletonScope();
			kernel.Bind<IGitService>().To<GitService>().InSingletonScope();
			kernel.Bind<IStock<GitProcess>>().To<Stock<GitProcess>>().InSingletonScope();
		}
	}
}
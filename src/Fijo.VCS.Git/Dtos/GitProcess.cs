using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace Fijo.VCS.Git.Dtos {
	[Dto]
	public class GitProcess {
		public readonly Process Process;
		public bool IsActive;
		public GitProcess(Process process) {
			Process = process;
		}
	}
}
using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using JetBrains.Annotations;

namespace Fijo.VCS.Git.Interfaces {
	[Facade]
	public interface IGitExecutionFacade {
		void Execute(string repoDict, string args, IEnumerable<string> inputs);

		[Pure]
		string TranslatePath(string path, bool makeAbsolute = true);
	}
}
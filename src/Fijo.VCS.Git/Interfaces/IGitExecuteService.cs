using System.Collections.Generic;
using Fijo.VCS.Git.Dtos;

namespace Fijo.VCS.Git.Interfaces {
	public interface IGitExecuteService {
		bool IsAvailible(GitProcess process);
		void Init(GitProcess process);
		void Execute(GitProcess process, string command);
		void ExecuteMany(GitProcess process, IEnumerable<string> commands);
		string ExecuteReturn(GitProcess process, string command);
		string ExecuteManyReturn(GitProcess process, IEnumerable<string> commands);
		void Close(GitProcess process);
	}
}
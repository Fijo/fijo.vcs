using Fijo.VCS.Git.Dtos;

namespace Fijo.VCS.Git.Interfaces {
	public interface IGitProcessFactory {
		GitProcess Create();
	}
}
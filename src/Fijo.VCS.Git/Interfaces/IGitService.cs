using System.Collections.Generic;

namespace Fijo.VCS.Git.Interfaces {
	public interface IGitService {
		void Add(string repositoryDictionary, IEnumerable<string> file);
		void Clone(string dictionary, string repository, string checkoutDir);
		void Commit(string repositoryDictionary, IEnumerable<string> files, string message);
		void Push(string repositoryDictionary, string mirror, string repository);
	}
}
using System.Diagnostics;
using Fijo.VCS.Git.Dtos;
using Fijo.VCS.Git.Interfaces;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;

namespace Fijo.VCS.Git.Factories {
	public class GitProcessFactory : IGitProcessFactory {
		private readonly string _binaryPath;
		private readonly string _binaryArgs;

		public GitProcessFactory() {
			var configurationService = Kernel.Resolve<IConfigurationService>();
			_binaryPath = configurationService.Get<string>("Fijo.VCS.Git.DashOrCygWinBinarySupportsGitPath");
			_binaryArgs = configurationService.Get<string>("Fijo.VCS.Git.DashOrCygWinArgs");
		}

		public GitProcess Create() {
			var procStartInfo = new ProcessStartInfo(_binaryPath, _binaryArgs)
			{
				RedirectStandardOutput = true,
				RedirectStandardInput = true,
				UseShellExecute = false,
				CreateNoWindow = false
			};
			var proc = new Process {StartInfo = procStartInfo};
			var connection = new GitProcess(proc);
			return connection;
		}
	}
}
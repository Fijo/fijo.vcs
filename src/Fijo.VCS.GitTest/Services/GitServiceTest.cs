using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Fijo.VCS.Git.Services;
using Fijo.VCS.GitTest.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;
using FijoCore.Infrastructure.TestTools.FAssert;
using NUnit.Framework;

namespace Fijo.VCS.GitTest.Services {
	[TestFixture]
	public class GitServiceTest {
		private readonly FAssert _fAssert = new FAssert();
		private GitService _gitService;
		private readonly string _halloText = string.Format("Hallo Text {0}", Guid.NewGuid().ToString());

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_gitService = new GitService();
		}
		
		private string GetRepoBaseDir() {
			return string.Format("repoCheckout{0}", Guid.NewGuid().ToString());
		}

		private string GetRepoDir(string repoBaseDir) {
			return Path.Combine(repoBaseDir, "Testrepo");
		}

		//[Test]
		public void CorrectRepoUrl_Clone_CreatesRepoDictionary() {
			lock(_gitService) Clone(GetRepoBaseDir());
		}

		private void Clone(string repoBaseDir) {
			if (Directory.Exists(repoBaseDir)) Directory.Delete(repoBaseDir, true);
			Directory.CreateDirectory(repoBaseDir);
			_gitService.Clone(repoBaseDir, Kernel.Resolve<IConfigurationService>().Get<string>("Fijo.VCS.GitTest.Services.GitServiceTest.TestRepository"), "Testrepo");
			var repoDir = GetRepoDir(repoBaseDir);
			_fAssert.WaitFor(100, 30000, () => Directory.Exists(repoDir));
		}
		
		//[Test]
		public void CorrectRepoUrl_Add_CreatesRepoDictionary() {
			lock(_gitService) Add(GetRepoBaseDir());
		}

		private void Add(string repoBaseDir) {
			Clone(repoBaseDir);
			var repoDir = GetRepoDir(repoBaseDir);
			var path = Path.Combine(repoDir, string.Format("{0}.txt", Guid.NewGuid().ToString()));
			File.WriteAllText(path, _halloText);
			_gitService.Add(repoDir, new List<string> {path});
			Thread.Sleep(500);
		}

		//[Test]
		public void CorrectRepoUrl_Commit_CreatesRepoDictionary() {
			lock(_gitService) Commit(GetRepoBaseDir());
		}

		private void Commit(string repoBaseDir) {
			Add(repoBaseDir);
			var repoDir = GetRepoDir(repoBaseDir);
			_gitService.Commit(repoDir, Enumerable.Empty<string>(), "git commit test (by GitServiceTest)");
			Thread.Sleep(1000);
		}

		//[Test]
		public void CorrectRepoUrl_Push_CreatesRepoDictionary() {
			lock(_gitService) Push(GetRepoBaseDir());
		}

		private void Push(string repoBaseDir) {
			Commit(repoBaseDir);
			var repoDir = GetRepoDir(repoBaseDir);
			_gitService.Push(repoDir, "origin", "master");
			Thread.Sleep(10000);
		}
	}
}
using Fijo.VCS.Git.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;

namespace Fijo.VCS.GitTest.Properties {
	public class InternalInitKernel : ExtendedInitKernel
	{
		public override void PreInit()
		{
			LoadModules(new GitInjectionModule());
		}
	}
}